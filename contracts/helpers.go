package contracts

import (
	"crypto/ecdsa"
	"crypto/sha256"
	"errors"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

// GetAtomicTokenAmount inputs an amount in human-readable tokens and outputs
// the same amount of TCRP in its smallest denomination
func GetAtomicTokenAmount(amount int64) *big.Int {
	tokens := big.NewInt(amount)
	multi := new(big.Int).Exp(big.NewInt(10), big.NewInt(TokenDecimals), nil)
	atomicAmount := new(big.Int).Mul(tokens, multi)

	return atomicAmount
}

// GetHumanTokenAmount takes an input amount in the smallest token denomination
// and returns a value in normal TCRP
func GetHumanTokenAmount(amount *big.Int) *big.Int {
	multi := new(big.Int).Exp(big.NewInt(10), big.NewInt(TokenDecimals), nil)
	humanAmount := new(big.Int).Div(amount, multi)

	return humanAmount
}

// GetListingHash converts a string twitter handle (without an @ symbol) into a
// listing hash
func GetListingHash(twitterHandle string) [32]byte {
	if twitterHandle == "obstropolos" {
		twitterHandle = "Obstropolos"
	} else if twitterHandle == "patberarducci" {
		twitterHandle = "\npatberarducci"
	} else if twitterHandle == "mattcorva" {
		twitterHandle = "\nmattcorva"
	}

	listingHash := sha256.Sum256([]byte(twitterHandle))

	// Convert that hash into the type it needs to be
	var txListingHash [32]byte
	copy(txListingHash[:], listingHash[0:32])
	return txListingHash
}

func getPublicAddress(privateKeyString string) (common.Address, error) {
	privateKey, err := crypto.HexToECDSA(privateKeyString)
	if err != nil {
		return common.Address{}, err
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return common.Address{}, errors.New("Could not convert public key to ECDSA")
	}

	address := crypto.PubkeyToAddress(*publicKeyECDSA)
	return address, nil
}

//func ensureTransactionSubmission(submit txSubmitter) (*types.Transaction, error) {
//var tx *types.Transaction
//var err error
//var timeout time.Duration = 5
//for {
//// If we've tried this many times it's probably time to give up
//if timeout == 20 {
//return nil, fmt.Errorf("transaction timed out with error: %s", err.Error())
//}

//// Make another attempt
//timeout++
//tx, err = submit()

//if err != nil && err.Error() == core.ErrReplaceUnderpriced.Error() {
//// Underpriced transaction, let's try again in a bit
//log.Printf("underpriced tx, trying again in %ds", timeout)
//time.Sleep(timeout * time.Second)
//continue
//} else if err != nil && err.Error() == core.ErrNonceTooLow.Error() {
//// Nonce is low, let's try again in a bit
//log.Printf("nonce too low, trying again in %ds", timeout)
//time.Sleep(timeout * time.Second)
//continue
//} else if err != nil {
//// Some other error
//return nil, err
//} else {
//// Success!
//break
//}
//}

//return tx, err
//}
