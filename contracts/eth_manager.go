package contracts

import (
	"os"

	"github.com/ethereum/go-ethereum/ethclient"
)

// ETHManager is our primary interface wrapper for the blockchain
type ETHManager struct {
	Client *ethclient.Client
	Reader *ethReader
	Writer *ethWriter
}

// NewETHManager instantiates a new tranasction manager struct
func NewETHManager() (*ETHManager, error) {
	ethSession, err := ethclient.Dial(os.Getenv("ETH_NODE_URI"))
	if err != nil {
		return nil, err
	}

	manager := &ETHManager{Client: ethSession}
	manager.Reader = newETHReader(manager)
	manager.Writer = newETHWriter(manager)

	return manager, nil
}
