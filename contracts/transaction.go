package contracts

import (
	"context"
	goErrors "errors"
	"log"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/core/types"
)

type txStatusType string

const (
	txStatusSubmitted      txStatusType = "submitted"
	txStatusRetrySubmitted txStatusType = "retrySubmitted"
	txStatusConfirmed                   = "confirmed"
	txStatusError                       = "error"
	txStatusReverted                    = "reverted"
)

var (
	// ErrTXTimedOut is returned when we've waited over ~2 minutes for a
	// transaction's confirmation but haven't gotten it
	ErrTXTimedOut = goErrors.New("tx timed out")

	// ErrTXReverted is returned when a transaction is confirmed but is
	// reverted due to an on-chain execution error
	ErrTXReverted = goErrors.New("tx reverted")
)

type asyncStatus struct {
	sync.Mutex
	cond  *sync.Cond
	Value bool
}

// Transaction represents a transaction that has been submitted to the
// blockchain
type Transaction struct {
	Manager    *ETHManager
	Submitter  TXSubmitter
	EthTX      *types.Transaction
	StatusChan chan *txStatus
	Attempts   uint64

	submittedStatus asyncStatus
	confirmedStatus asyncStatus
	failed          bool
	err             error
}

type txStatus struct {
	Type  txStatusType
	TX    *types.Transaction
	Error error
}

func newTransaction(manager *ETHManager, submitter TXSubmitter) *Transaction {
	tx := &Transaction{
		Manager:    manager,
		Submitter:  submitter,
		StatusChan: make(chan *txStatus),
		confirmedStatus: asyncStatus{
			Value: false,
		},
		submittedStatus: asyncStatus{
			Value: false,
		},
	}

	tx.confirmedStatus.cond = sync.NewCond(&tx.confirmedStatus)
	tx.submittedStatus.cond = sync.NewCond(&tx.submittedStatus)

	go tx.HandleLifecycle()

	return tx
}

// AwaitConfirmation blocks until the transaction has received a confirmation
// on the blockchain or errors/times out
func (tx *Transaction) AwaitConfirmation() error {
	if tx.confirmedStatus.Value {
		return nil
	}

	// We're not confirmed yet so let's wait on the channel
	tx.confirmedStatus.Lock()
	tx.confirmedStatus.cond.Wait()
	defer tx.confirmedStatus.Unlock()

	// Did we fail or succeed?
	if !tx.confirmedStatus.Value && tx.err == nil {
		panic("Transaction was not confirmed but no error found!")
	} else if tx.failed {
		return tx.err
	}

	return nil
}

// AwaitSubmit blocks until the transaction has been submitted successfully or
// errors/times out
func (tx *Transaction) AwaitSubmit() error {
	if tx.submittedStatus.Value {
		return nil
	}

	// Wait for the transaction to be submitted
	tx.submittedStatus.Lock()
	tx.submittedStatus.cond.Wait()
	defer tx.submittedStatus.Unlock()

	// Did we fail or succeed?
	if tx.submittedStatus.Value {
		return nil
	} else if !tx.submittedStatus.Value && tx.err == nil {
		panic("Transaction was not submitted but no error found!")
	}

	return tx.err
}

// HandleLifecycle listens on the transaction's status channel and updates its
// state accordingly
func (tx *Transaction) HandleLifecycle() {
	// Listen on the update channel for a submission or error event
	for update := range tx.StatusChan {
		switch update.Type {
		case txStatusSubmitted:
			tx.EthTX = update.TX
			tx.markSubmitted(true)
			go tx.awaitConfirmation()
			break

		case txStatusError:
			log.Printf("TX error: %s", update.Error.Error())
			if tx.Attempts >= 10 {
				log.Println("TX reached maximum attempts, aborting")
				tx.err = update.Error
				tx.markConfirmed(false)
				tx.markSubmitted(false)
				return
			}

			// If the transaction timed out after submission this could be an
			// eth issue, so let's give it one more shot and then give up
			if update.Error == ErrTXTimedOut {
				tx.Attempts = 9
				tx.Manager.Writer.ResubmitTX(tx)
				return
			}

			tx.err = update.Error

			// Wait a little bit and resubmit
			time.Sleep(5 * time.Second)
			tx.Manager.Writer.ResubmitTX(tx)

		case txStatusConfirmed:
			tx.markConfirmed(true)
			return
		}
	}
}

// Hex returns the hex value of the eth transaction's hash
func (tx *Transaction) Hex() string {
	if tx.EthTX == nil {
		return ""
	}

	return tx.EthTX.Hash().Hex()
}

func (tx *Transaction) awaitConfirmation() {
	emptyCtx := context.Background()
	ctx, cancel := context.WithDeadline(emptyCtx, time.Now().Add(time.Minute*3))
	defer cancel()

	var retryDelay time.Duration = 16
	for {
		receipt, err := tx.Manager.Client.TransactionReceipt(ctx, tx.EthTX.Hash())

		// Wait at most ~2 minutes for confimation
		if err != nil && retryDelay > 128 {
			tx.StatusChan <- &txStatus{txStatusError, nil, ErrTXTimedOut}
			return
		} else if err == ethereum.NotFound {
			time.Sleep(retryDelay * time.Second)
			retryDelay = retryDelay * 2
			continue
		} else if err != nil {
			tx.StatusChan <- &txStatus{txStatusError, nil, err}
			return
		}

		// Did the tx succeed?
		if receipt.Status == types.ReceiptStatusFailed {
			tx.StatusChan <- &txStatus{txStatusError, nil, ErrTXReverted}
		} else {
			tx.StatusChan <- &txStatus{txStatusConfirmed, nil, nil}
		}
	}
}

func (tx *Transaction) markConfirmed(status bool) {
	tx.confirmedStatus.Lock()
	tx.confirmedStatus.Value = status
	tx.confirmedStatus.Unlock()
	tx.confirmedStatus.cond.Broadcast()
}

func (tx *Transaction) markSubmitted(status bool) {
	tx.submittedStatus.Lock()
	tx.submittedStatus.Value = status
	tx.submittedStatus.Unlock()
	tx.submittedStatus.cond.Broadcast()
}
