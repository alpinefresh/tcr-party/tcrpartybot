package contracts

import (
	"context"
	"errors"
	"math/big"
	"os"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
)

const (
	// TokenDecimals is the number you can multiply/divide by in order to
	// arrive at a human readable TCRP balance
	TokenDecimals = 15
)

// RegistryListing represents a listing on the TCR
type RegistryListing struct {
	ListingHash       [32]byte
	ApplicationExpiry *big.Int
	Whitelisted       bool
	Owner             common.Address
	UnstakedDeposit   *big.Int
	ChallengeID       *big.Int
	ExitTime          *big.Int
	ExitTimeExpiry    *big.Int
}

// Poll represents a poll conducted on the PLCR voting contract
type Poll struct {
	CommitEndDate *big.Int
	RevealEndDate *big.Int
	VoteQuorum    *big.Int
	VotesFor      *big.Int
	VotesAgainst  *big.Int
}

// Challenge represents the challenge struct on the Registry contract
type Challenge struct {
	RewardPool  *big.Int
	Challenger  common.Address
	Resolved    bool
	Stake       *big.Int
	TotalTokens *big.Int
}

// ethReader facilitates reading data from the blockchain
type ethReader struct {
	manager *ETHManager
}

func newETHReader(manager *ETHManager) *ethReader {
	return &ethReader{manager}
}

// TCRPGetBalance returns the balance of a given wallet address on the TCRP
// contract
func (r *ethReader) TCRPGetBalance(address string) (*big.Int, error) {
	tokenAddress := common.HexToAddress(os.Getenv("TOKEN_ADDRESS"))
	token, err := NewTCRPartyPoints(tokenAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	balance, err := token.BalanceOf(nil, common.HexToAddress(address))
	if err != nil {
		return nil, err
	}

	return balance, nil
}

// GetListing returns a listing from the TCR based on a [32]byte hash
func (r *ethReader) GetListing(listingHash [32]byte) (*RegistryListing, error) {
	registryAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	registry, err := NewRegistry(registryAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	result, err := registry.Listings(nil, listingHash)
	if err != nil {
		return nil, err
	}

	if result.Owner.Big().Cmp(big.NewInt(0)) == 0 {
		return nil, nil
	}

	listing := RegistryListing{
		ListingHash:       listingHash,
		ApplicationExpiry: result.ApplicationExpiry,
		Whitelisted:       result.Whitelisted,
		Owner:             result.Owner,
		UnstakedDeposit:   result.UnstakedDeposit,
		ChallengeID:       result.ChallengeID,
		ExitTime:          result.ExitTime,
		ExitTimeExpiry:    result.ExitTimeExpiry,
	}
	return &listing, nil
}

// GetListingFromHandle returns a listing from the TCR based on a twitter handle
func (r *ethReader) GetListingFromHandle(twitterHandle string) (*RegistryListing, error) {
	// Generate a listing hash from the handle's string value
	listingHash := GetListingHash(twitterHandle)
	return r.GetListing(listingHash)
}

// GetApplicationsByAddress retrieves a list of all applications made by the
// given address. Note that these applications may or may not still be active,
// as we are only returning a list of _Application events. Further processing
// is required in order to get a list of active listings on the registry.
func (r *ethReader) GetApplicationsByAddress(address string) ([]*RegistryApplication, error) {
	blockCursor := new(big.Int)
	fromBlock, ok := blockCursor.SetString(os.Getenv("START_BLOCK"), 10)
	if !ok {
		return nil, errors.New("Could not set fromBlock while getting active registry listings")
	}

	query := ethereum.FilterQuery{
		Addresses: []common.Address{
			common.HexToAddress(os.Getenv("TCR_ADDRESS")),
		},
		FromBlock: fromBlock,
		Topics: [][]common.Hash{
			{common.HexToHash(applicationTopicHash)},
		},
	}

	logs, err := r.manager.Client.FilterLogs(context.Background(), query)
	if err != nil {
		return nil, err
	}

	events := []*RegistryApplication{}
	for _, event := range logs {
		event, err := DecodeApplicationEvent(event.Topics, event.Data)
		if err != nil {
			return nil, err
		} else if event.Applicant.Hex() != address {
			continue
		}

		events = append(events, event)
	}

	return events, nil
}

func (r *ethReader) getApplicationEventFromHash(listingHash [32]byte) (*RegistryApplication, error) {
	blockCursor := new(big.Int)
	fromBlock, ok := blockCursor.SetString(os.Getenv("START_BLOCK"), 10)
	if !ok {
		return nil, errors.New("Could not set fromBlock while getting active registry listings")
	}

	// Since listing data is only broadcast once (in the
	// initial application event), we need to filter out the specific event we're
	// looking for in order to fetch the associated data field.
	query := ethereum.FilterQuery{
		Addresses: []common.Address{
			common.HexToAddress(os.Getenv("TCR_ADDRESS")),
		},
		FromBlock: fromBlock,
		Topics: [][]common.Hash{
			{common.HexToHash(applicationTopicHash)},
			{common.HexToHash(common.Bytes2Hex(listingHash[:]))},
		},
	}

	logs, err := r.manager.Client.FilterLogs(context.Background(), query)
	if err != nil {
		return nil, err
	}

	var latestEvent *RegistryApplication
	for _, event := range logs {
		event, err := DecodeApplicationEvent(event.Topics, event.Data)
		if err != nil {
			return nil, err
		}

		latestEvent = event
	}

	return latestEvent, nil
}

// GetListingDataFromHash returns the data field (ideally a Twitter
// handle) from a given listing hash.
func (r *ethReader) GetListingDataFromHash(listingHash [32]byte) (string, error) {
	applicationEvent, err := r.getApplicationEventFromHash(listingHash)
	if err != nil {
		return "", err
	} else if applicationEvent == nil {
		return "", nil
	}

	return applicationEvent.Data, nil
}

// GetListingOwnerFromHash returns the applicant field from the most recent
// Application event for the given listing hash. This is mostly useful for
// retreiving information about a listing that has been removed from the list.
func (r *ethReader) GetListingOwnerFromHash(listingHash [32]byte) (*common.Address, error) {
	applicationEvent, err := r.getApplicationEventFromHash(listingHash)
	if err != nil {
		return nil, err
	} else if applicationEvent == nil {
		return nil, nil
	}

	return &applicationEvent.Applicant, nil
}

// GetListingHashFromChallenge returns the listing hash being challenged given a
// challenge's ID. This is useful for RewardClaimed events, where the challenge
// ID is provided by the listing hash is not.
func (r *ethReader) GetListingHashFromChallenge(challengeID *big.Int) ([32]byte, error) {
	blockCursor := new(big.Int)
	fromBlock, ok := blockCursor.SetString(os.Getenv("START_BLOCK"), 10)
	if !ok {
		return [32]byte{}, errors.New("Could not set fromBlock while getting active registry listings")
	}

	// Since listing data is only broadcast once (in the
	// initial application event), we need to filter out the specific event we're
	// looking for in order to fetch the associated data field.
	query := ethereum.FilterQuery{
		Addresses: []common.Address{
			common.HexToAddress(os.Getenv("TCR_ADDRESS")),
		},
		FromBlock: fromBlock,
		Topics: [][]common.Hash{
			{common.HexToHash(challengeTopicHash)},
		},
	}

	logs, err := r.manager.Client.FilterLogs(context.Background(), query)
	if err != nil {
		return [32]byte{}, err
	}

	var latestEvent *RegistryChallenge
	for _, event := range logs {
		event, err := DecodeChallengeEvent(event.Topics, event.Data)
		if err != nil {
			return [32]byte{}, err
		}

		if event.ChallengeID.Cmp(challengeID) == 0 {
			latestEvent = event
		}
	}

	if latestEvent == nil {
		return [32]byte{}, ErrCannotFindListingHash
	}

	return latestEvent.ListingHash, nil
}

// GetPLCRContractAddress returns the address of the PLCR voting contract,
// fetched directly from the registry contract
func (r *ethReader) GetPLCRContractAddress() (common.Address, error) {
	// Fetch the address of the PLCR voting contract
	registryAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	registry, err := NewRegistry(registryAddress, r.manager.Client)
	if err != nil {
		return common.Address{}, err
	}

	return registry.Voting(nil)
}

// GetAllListings returns a list of registry listings
func (r *ethReader) GetAllListings() ([]*RegistryListing, error) {
	blockCursor := new(big.Int)
	fromBlock, ok := blockCursor.SetString(os.Getenv("START_BLOCK"), 10)
	if !ok {
		return nil, errors.New("Could not set fromBlock while getting active registry listings")
	}

	// Filter all _Application events
	query := ethereum.FilterQuery{
		Addresses: []common.Address{
			common.HexToAddress(os.Getenv("TCR_ADDRESS")),
		},
		FromBlock: fromBlock,
		Topics: [][]common.Hash{
			{common.HexToHash(applicationTopicHash)},
		},
	}

	logs, err := r.manager.Client.FilterLogs(context.Background(), query)
	if err != nil {
		return nil, err
	}

	var listings []*RegistryListing
	seenListings := map[[32]byte]bool{}
	for _, event := range logs {
		// Get the listing hash
		var listingHash [32]byte
		copy(listingHash[:], event.Topics[1].Bytes()[0:32])

		// If we've already seen this listing before, move on
		if seenListings[listingHash] {
			continue
		}

		// Make sure this listing still exists
		listing, err := r.GetListing(listingHash)
		if err != nil {
			return nil, err
		} else if listing == nil {
			continue
		}

		listings = append(listings, listing)
		seenListings[listingHash] = true
	}

	return listings, nil
}

// GetWhitelistedListings returns a list of all listings that are active
// members of the TCR.
func (r *ethReader) GetWhitelistedListings() ([]*RegistryListing, error) {
	listings, err := r.GetAllListings()
	if err != nil {
		return nil, err
	}

	var activeListings []*RegistryListing
	for _, listing := range listings {
		if listing.Whitelisted {
			activeListings = append(activeListings, listing)
		}
	}

	return activeListings, nil
}

// GetWhitelistedHandles fetches a list of twitter handles currently
// whitelisted on the TCR
func (r *ethReader) GetWhitelistedHandles() ([]string, error) {
	listings, err := r.GetWhitelistedListings()
	if err != nil {
		return nil, err
	}

	handles := make([]string, len(listings))
	for i, listing := range listings {
		handle, err := r.GetListingDataFromHash(listing.ListingHash)
		if err != nil {
			return nil, err
		}

		handles[i] = handle
	}

	return handles, nil
}

// GetPoll returns a poll from the PLCR voting contract given a challenge ID
func (r *ethReader) GetPoll(pollID *big.Int) (*Poll, error) {
	plcrAddress, err := r.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	plcr, err := NewPLCRVoting(plcrAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	pollData, err := plcr.PollMap(nil, pollID)
	if err != nil {
		return nil, err
	} else if pollData.CommitEndDate == nil {
		return nil, nil
	}

	poll := Poll(pollData)
	return &poll, nil
}

// GetChallenge returns a challenge struct given its ID
func (r *ethReader) GetChallenge(challengeID *big.Int) (*Challenge, error) {
	registry, err := NewRegistry(common.HexToAddress(os.Getenv("TCR_ADDRESS")), r.manager.Client)
	if err != nil {
		return nil, err
	}

	challengeData, err := registry.Challenges(nil, challengeID)
	if err != nil {
		return nil, err
	}

	challenge := Challenge(challengeData)
	return &challenge, nil
}

// PLCRLockedTokens returns the number of tokens that are currently locked up
// for voting.
func (r *ethReader) PLCRLockedTokens(multisigAddress string) (*big.Int, error) {
	plcrAddress, err := r.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	plcr, err := NewPLCRVoting(plcrAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	return plcr.GetLockedTokens(nil, common.HexToAddress(multisigAddress))
}

// PLCRLockedChallengeID returns the ID of the challenge which has the most
// amount of tokens locked up
func (r *ethReader) PLCRLockedChallengeID(multisigAddress string) (*big.Int, error) {
	plcrAddress, err := r.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	plcr, err := NewPLCRVoting(plcrAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	return plcr.GetLastNode(nil, common.HexToAddress(multisigAddress))
}

// PLCRGetBalance returns the amount of tokens deposited into the voting
// contract
func (r *ethReader) PLCRGetBalance(address string) (*big.Int, error) {
	// Fetch the PLCR contract's address
	plcrAddress, err := r.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	plcr, err := NewPLCRVoting(plcrAddress, r.manager.Client)
	if err != nil {
		return nil, err
	}

	balance, err := plcr.VoteTokenBalance(nil, common.HexToAddress(address))
	if err != nil {
		return nil, err
	}

	return balance, nil
}

// ApplicationWasMade returns true or false depending on whether or not a
// twitter handle is already an application or listing on the registry
func (r *ethReader) ApplicationWasMade(twitterHandle string) (bool, error) {
	listingHash := GetListingHash(twitterHandle)

	registryAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	registry, err := NewRegistry(registryAddress, r.manager.Client)
	if err != nil {
		return false, err
	}

	result, err := registry.AppWasMade(nil, listingHash)
	if err != nil {
		return false, err
	}

	return result, nil
}
