package contracts

import (
	"context"
	"crypto/ecdsa"
	"log"
	"math/big"
	"os"
	"strconv"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
)

const (
	gasLimit = 5000000
)

type ethWriter struct {
	Manager   *ETHManager
	txQueue   chan *Transaction
	nextNonce uint64
}

func newETHWriter(manager *ETHManager) *ethWriter {
	return &ethWriter{
		Manager: manager,
		txQueue: make(chan *Transaction, 100),
	}
}

// HandleTransactions blocks the current goroutine, listening on the txQueue
// and handling the submission/monitoring of transations being sent to that 'ol
// block and chain.
func (w *ethWriter) HandleTransactions(errChan chan<- error) {
	for tx := range w.txQueue {
		tx.Attempts++

		txOpts, err := w.setupTXOpts()
		if err != nil {
			tx.StatusChan <- &txStatus{txStatusError, nil, errors.Wrap(err)}
			continue
		}

		ethTX, err := tx.Submitter.Submit(w, txOpts)
		if err != nil {
			tx.StatusChan <- &txStatus{txStatusError, nil, errors.Wrap(err)}
			continue
		}

		tx.StatusChan <- &txStatus{txStatusSubmitted, ethTX, nil}
	}

	log.Println("HandleTransactions exiting")
}

// SubmitTX submits a transaction to the blockchain and ensures its confirmation
func (w *ethWriter) SubmitTX(submitter interface{}) (*Transaction, error) {
	switch s := submitter.(type) {
	case TXSubmitter:
		tx := newTransaction(w.Manager, s)
		w.txQueue <- tx

		err := tx.AwaitSubmit()
		return tx, err
	case TXSequenceSubmitter:
		sequence, err := s.GenerateSequence(w)
		var finalTX *Transaction

		if err != nil {
			return nil, err
		}

		for _, sub := range sequence {
			tx := newTransaction(w.Manager, sub)
			w.txQueue <- tx

			if err := tx.AwaitSubmit(); err != nil {
				return tx, err
			}

			finalTX = tx
		}

		return finalTX, nil
	default:
		panic("SubmitTX must receive a TXSubmitter or TXSequenceGenerator")
	}
}

// ResubmitTX resubmits a given transaction to the blockchain with a new nonce
func (w *ethWriter) ResubmitTX(tx *Transaction) {
	w.txQueue <- tx
}

func (w *ethWriter) fetchNonce() (*big.Int, error) {
	if w.nextNonce != 0 {
		return big.NewInt(int64(w.nextNonce)), nil
	}

	privateKey, err := crypto.HexToECDSA(os.Getenv("MASTER_PRIVATE_KEY"))
	if err != nil {
		return nil, err
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, errors.New("could not convert public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := w.Manager.Client.NonceAt(context.Background(), fromAddress, nil)
	if err != nil {
		return nil, err
	}

	w.nextNonce = nonce

	return big.NewInt(int64(nonce)), nil
}

func (w *ethWriter) setupTXOpts() (*bind.TransactOpts, error) {
	privateKey, err := crypto.HexToECDSA(os.Getenv("MASTER_PRIVATE_KEY"))
	if err != nil {
		return nil, err
	}

	gasPrice, err := w.Manager.Client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, err
	}

	// Add a bit more to the gas price to increase the chances the tx will get
	// picked up.
	gasModifierStr := os.Getenv("GAS_MODIFIER")
	if gasModifierStr != "" {
		gasModifier, err := strconv.ParseInt(gasModifierStr, 10, 64)
		if err != nil {
			return nil, errors.Errorf("could not parse GAS_MODIFIER; check your environment")
		}

		gasPrice.Add(gasPrice, big.NewInt(gasModifier))
	}

	nonce, err := w.fetchNonce()
	if err != nil {
		return nil, err
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Value = big.NewInt(0)
	auth.Nonce = nonce
	auth.GasLimit = uint64(gasLimit)
	auth.GasPrice = gasPrice

	w.nextNonce++

	return auth, nil
}
