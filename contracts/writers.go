package contracts

import (
	"errors"
	"fmt"
	"log"
	"math/big"
	"os"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"golang.org/x/crypto/sha3"
)

const (
	applicationTopicHash = "0xa27f550c3c7a7c6d8369e5383fdc7a3b4850d8ce9e20066f9d496f6989f00864"
	challengeTopicHash   = "0xf98a08756a3603420a080d66764f73deb1e30896c315cfed03e17f88f5eb30f7"
)

// ErrCannotFindListingHash is returned when an event being searched for via its
// listing hash cannot be found
var ErrCannotFindListingHash = errors.New("cannot find listing hash")

// TXSubmitter provides an interface for structs that can submit transactions
// to the blockchain
type TXSubmitter interface {
	Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error)
}

// TXSequenceSubmitter provides an interface for easily submitting sequences of
// transactions to the blockchain
type TXSequenceSubmitter interface {
	GenerateSequence(w *ethWriter) ([]TXSubmitter, error)
}

// TCRPMintTX assigns new tokens to the given ETH address
type TCRPMintTX struct {
	To     string
	Amount *big.Int
}

// Submit submits the mint transaction
func (params *TCRPMintTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	contractAddress := common.HexToAddress(os.Getenv("TOKEN_ADDRESS"))
	token, err := NewTCRPartyPoints(contractAddress, w.Manager.Client)
	if err != nil {
		return nil, err
	}

	txAddress := common.HexToAddress(params.To)
	return token.Mint(txOpts, txAddress, params.Amount)
}

// TCRPApproveTX approves tokens for a given ETH address to spend
type TCRPApproveTX struct {
	MultisigAddress string
	SpenderAddress  string
	Amount          *big.Int
}

// Submit submits the approve transaction
func (params *TCRPApproveTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	log.Printf("Approving %d TCRP for spender %s", GetHumanTokenAmount(params.Amount).Int64(), params.SpenderAddress)
	tokenAddress := common.HexToAddress(os.Getenv("TOKEN_ADDRESS"))

	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              tokenAddress,
		ABIString:       TCRPartyPointsABI,
		Method:          "approve",
		Args: []interface{}{
			common.HexToAddress(params.SpenderAddress),
			params.Amount,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}

// RegistryApplyTX nominates a twitter handle for inclusion on the TCR
type RegistryApplyTX struct {
	MultisigAddress string
	Amount          *big.Int
	TwitterHandle   string
}

// GenerateSequence submits the nominate transaction sequence
func (params *RegistryApplyTX) GenerateSequence(w *ethWriter) ([]TXSubmitter, error) {
	contractAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	approvalTX := &TCRPApproveTX{
		MultisigAddress: params.MultisigAddress,
		SpenderAddress:  contractAddress.Hex(),
		Amount:          params.Amount,
	}

	listingHash := GetListingHash(params.TwitterHandle)
	applyTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              contractAddress,
		ABIString:       RegistryABI,
		Method:          "apply",
		Args: []interface{}{
			listingHash,
			params.Amount,
			params.TwitterHandle,
		},
	}

	return []TXSubmitter{approvalTX, applyTX}, nil
}

// RegistryWithdrawTX calls the withdraw method on the registry contract,
// taking unstaked tokens out of the registry and returning it to a listing's
// owner.
type RegistryWithdrawTX struct {
	TwitterHandle string
	Amount        *big.Int
}

// Submit submits the withdraw transaction
func (params *RegistryWithdrawTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	listingHash := GetListingHash(params.TwitterHandle)
	listing, err := w.Manager.Reader.GetListing(listingHash)
	if err != nil {
		return nil, err
	} else if listing == nil {
		return nil, fmt.Errorf("no listing for %s", params.TwitterHandle)
	}

	log.Printf("Withdrawing %d tokens from listing %s for %s", GetHumanTokenAmount(params.Amount), params.TwitterHandle, listing.Owner.Hash().Hex())

	contractAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	proxiedTX := &MSWSubmitTX{
		MultisigAddress: listing.Owner.Hash().Hex(),
		To:              contractAddress,
		ABIString:       RegistryABI,
		Method:          "withdraw",
		Args: []interface{}{
			listingHash,
			params.Amount,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}

// RegistryClaimVoterRewardTX calls the claimVoterReward method on the registry
// contract in order to retrieve tokens rewarded after voting on the winning
// side of a challenge.
type RegistryClaimVoterRewardTX struct {
	MultisigAddress string
	PollID          *big.Int
}

// Submit submits the claim transaction
func (params *RegistryClaimVoterRewardTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	contractAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              contractAddress,
		ABIString:       RegistryABI,
		Method:          "claimReward",
		Args: []interface{}{
			params.PollID,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}

// RegistryChallengeTX creates a new challenge to a listing on the TCR
type RegistryChallengeTX struct {
	MultisigAddress string
	Amount          *big.Int
	TwitterHandle   string
}

// GenerateSequence submits the challenge transaction sequence
func (params *RegistryChallengeTX) GenerateSequence(w *ethWriter) ([]TXSubmitter, error) {
	contractAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))

	approvalTX := &TCRPApproveTX{
		MultisigAddress: params.MultisigAddress,
		SpenderAddress:  contractAddress.Hex(),
		Amount:          params.Amount,
	}

	listingHash := GetListingHash(params.TwitterHandle)
	challengeTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              contractAddress,
		ABIString:       RegistryABI,
		Method:          "challenge",
		Args: []interface{}{
			listingHash,
			params.TwitterHandle,
		},
	}

	return []TXSubmitter{approvalTX, challengeTX}, nil
}

// WalletFactoryDeployTX creates a new instance of the wallet factory
type WalletFactoryDeployTX struct {
	Identifier *big.Int
}

// Submit submits the deploy transaction
func (params *WalletFactoryDeployTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	contractAddress := common.HexToAddress(os.Getenv("WALLET_FACTORY_ADDRESS"))
	factory, err := NewMultiSigWalletFactory(contractAddress, w.Manager.Client)
	if err != nil {
		return nil, err
	}

	botKey, err := getPublicAddress(os.Getenv("MASTER_PRIVATE_KEY"))
	if err != nil {
		return nil, err
	}

	owners := []common.Address{botKey}
	return factory.Create(txOpts, owners, big.NewInt(1), params.Identifier)
}

// MSWSubmitTX calls the submitTransaction method on a user's multisig
// wallet, allowing us to proxy transactions through their wallet addresss.
type MSWSubmitTX struct {
	MultisigAddress string
	ABIString       string
	Method          string
	Args            []interface{}
	To              common.Address
	Value           *big.Int
}

// Submit submits the proxied transaction
func (params *MSWSubmitTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	contractAddress := common.HexToAddress(params.MultisigAddress)
	wallet, err := NewMultiSigWallet(contractAddress, w.Manager.Client)
	if err != nil {
		return nil, err
	}

	if params.Value == nil {
		params.Value = big.NewInt(0)
	}

	// Parse the ABI string and pack the arguments
	parsed, err := abi.JSON(strings.NewReader(params.ABIString))
	data, err := parsed.Pack(params.Method, params.Args...)

	return wallet.SubmitTransaction(txOpts, params.To, params.Value, data)
}

// RegistryUpdateStatusTX calls updateStatus on the registry contract, allowing
// a listing past its application period (or past a challenge's reveal period)
// to be promoted to a whitelisted listing or kicked off of the list.
type RegistryUpdateStatusTX struct {
	ListingHash [32]byte
}

// Submit submits the updateStatus transaction
func (params *RegistryUpdateStatusTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	registryAddress := common.HexToAddress(os.Getenv("TCR_ADDRESS"))
	registry, err := NewRegistry(registryAddress, w.Manager.Client)
	if err != nil {
		return nil, err
	}

	return registry.UpdateStatus(txOpts, params.ListingHash)
}

// PLCRDepositTX locks up a number of tokens in the TCR's PLCR voting contract
type PLCRDepositTX struct {
	MultisigAddress string
	Amount          *big.Int
}

// GenerateSequence submits the deposit transaction sequence
func (params *PLCRDepositTX) GenerateSequence(w *ethWriter) ([]TXSubmitter, error) {
	log.Printf("Depositing %d into PLCR contract for %s", GetHumanTokenAmount(params.Amount).Int64(), params.MultisigAddress)
	plcrAddress, err := w.Manager.Reader.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	approvalTX := &TCRPApproveTX{
		MultisigAddress: params.MultisigAddress,
		SpenderAddress:  plcrAddress.Hex(),
		Amount:          params.Amount,
	}

	depositTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              plcrAddress,
		ABIString:       PLCRVotingABI,
		Method:          "requestVotingRights",
		Args: []interface{}{
			params.Amount,
		},
	}

	return []TXSubmitter{approvalTX, depositTX}, nil
}

// PLCRCommitVoteTX calls the commitVote method on the PLCR voting contract
type PLCRCommitVoteTX struct {
	MultisigAddress string
	PollID          *big.Int
	Amount          *big.Int
	// Salt should be a random integer. You can generate this by calling
	// rand.Int63() and converting it to a *big.Int
	Salt *big.Int
	Vote bool
}

// Submit submits the commit transaction
func (params *PLCRCommitVoteTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	plcrAddress, err := w.Manager.Reader.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	// Find the insert point in the DLL
	plcrVoting, err := NewPLCRVoting(plcrAddress, w.Manager.Client)
	if err != nil {
		return nil, err
	}

	voteOption := int64(0)
	if params.Vote {
		voteOption = 1
	}

	voterAddress := common.HexToAddress(params.MultisigAddress)
	prevNode, err := plcrVoting.GetInsertPointForNumTokens(nil, voterAddress, params.Amount, params.PollID)
	if err != nil {
		return nil, err
	}

	// Generating out secret voting hash is a bit of a process...
	uintType, err := abi.NewType("uint256", nil)
	if err != nil {
		return nil, err
	}

	arguments := abi.Arguments{
		{Type: uintType},
		{Type: uintType},
	}

	hashBytes, err := arguments.Pack(big.NewInt(voteOption), params.Salt)
	if err != nil {
		return nil, err
	}

	// Hash the packed arguments
	var secretHashBuf []byte
	hashing := sha3.NewLegacyKeccak256()
	hashing.Write(hashBytes)
	secretHashBuf = hashing.Sum(secretHashBuf)

	// Convert from []byte to [32]byte
	var secretHash [32]byte
	copy(secretHash[:], secretHashBuf[0:32])

	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              plcrAddress,
		ABIString:       PLCRVotingABI,
		Method:          "commitVote",
		Args: []interface{}{
			params.PollID,
			secretHash,
			params.Amount,
			prevNode,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}

// PLCRRevealVote reveals a previously committed vote on the PLCR contract
type PLCRRevealVoteTX struct {
	MultisigAddress string
	PollID          *big.Int
	Salt            *big.Int
	Vote            bool
}

// Submit submits the reveal transaction
func (params *PLCRRevealVoteTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	plcrAddress, err := w.Manager.Reader.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	intVote := int64(0)
	if params.Vote {
		intVote = 1
	}

	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              plcrAddress,
		ABIString:       PLCRVotingABI,
		Method:          "revealVote",
		Args: []interface{}{
			params.PollID,
			big.NewInt(intVote),
			params.Salt,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}

// PLCRRescueTokensTX rescues tokens locked in a poll where the user's vote was
// never revealed
type PLCRRescueTokensTX struct {
	MultisigAddress string
	PollID          *big.Int
}

// Submit submits the rescue tokens transaction
func (params *PLCRRescueTokensTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	plcrAddress, err := w.Manager.Reader.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              plcrAddress,
		ABIString:       PLCRVotingABI,
		Method:          "rescueTokens",
		Args:            []interface{}{params.PollID},
	}

	return proxiedTX.Submit(w, txOpts)

}

// PLCRWithdraw withdraws tokens from the contract and returns it to the user's
// multisig wallet
type PLCRWithdrawTX struct {
	MultisigAddress string
	Amount          *big.Int
}

// Submit submits the withdraw transaction
func (params *PLCRWithdrawTX) Submit(w *ethWriter, txOpts *bind.TransactOpts) (*types.Transaction, error) {
	plcrAddress, err := w.Manager.Reader.GetPLCRContractAddress()
	if err != nil {
		return nil, err
	}

	log.Printf("Withdrawing %d from PLCR contract for %s", GetHumanTokenAmount(params.Amount).Int64(), params.MultisigAddress)

	proxiedTX := &MSWSubmitTX{
		MultisigAddress: params.MultisigAddress,
		To:              plcrAddress,
		ABIString:       PLCRVotingABI,
		Method:          "withdrawVotingRights",
		Args: []interface{}{
			params.Amount,
		},
	}

	return proxiedTX.Submit(w, txOpts)
}
