package main

import (
	"log"
	"math/big"
	"math/rand"

	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
)

func deployWallet(eth *contracts.ETHManager, errChan chan<- error) {
	identifier := rand.Int63()
	tx, err := eth.Writer.SubmitTX(&contracts.WalletFactoryDeployTX{
		Identifier: big.NewInt(identifier),
	})
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}

	log.Printf("TX: %s ID: %d", tx.Hex(), identifier)
}

func deleteAccount(twitterHandle string) error {
	account, err := models.FindAccountByHandle(twitterHandle)
	if err != nil {
		return err
	} else if account == nil {
		return errors.Errorf("Could not find account for handle %s", twitterHandle)
	}

	return account.Destroy()
}
