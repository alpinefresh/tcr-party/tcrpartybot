package main

import (
	"log"
	"math/big"

	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
)

func runJob(eth *contracts.ETHManager, errChan chan<- error, job string) {
	switch job {
	case "revealUnrevealedVotes":
		revealUnrevealedVotes(eth, errChan)
	}
}

func revealUnrevealedVotes(eth *contracts.ETHManager, errChan chan<- error) {
	log.Println("Rescuing votes...")

	lostVotes, err := models.FindLostVotes()
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}

	for _, vote := range lostVotes {
		// Find the associated account
		account, err := models.FindAccountByID(vote.AccountID)
		if err != nil {
			errChan <- errors.Wrap(err)
			continue
		}

		if !account.HasMultisigWallet() {
			log.Println("Skipping account with no multisig address")
			continue
		}

		tx, err := eth.Writer.SubmitTX(&contracts.PLCRRescueTokensTX{
			MultisigAddress: account.MultisigAddress.String,
			PollID:          big.NewInt(vote.PollID),
		})

		log.Printf("Rescuing vote %d:%d tx: %s", vote.PollID, vote.AccountID, tx.Hex())

		if err := tx.AwaitConfirmation(); err != nil {
			errChan <- errors.Wrap(err)
			continue
		}

		if err := vote.MarkRescued(); err != nil {
			errChan <- errors.Wrap(err)
			continue
		}
	}

	log.Println("Done rescuing votes")
}
