package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/alpinefresh/tcrpartybot/api"
	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/events"
	"gitlab.com/alpinefresh/tcrpartybot/models"
	"gitlab.com/alpinefresh/tcrpartybot/twitter"
)

func main() {
	// Some pre-boot config
	rand.Seed(time.Now().UnixNano())
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))
	godotenv.Load()

	// Start the db connection pool
	models.GetDBSession()

	// Check to see if we have credentials for the two twitter handles
	_, err := models.FindOAuthTokenByHandle(os.Getenv("PARTY_BOT_HANDLE"))
	if err != nil {
		log.Printf("Credentials for party bot not found. Please authenticate!")
	}

	_, err = models.FindOAuthTokenByHandle(os.Getenv("VIP_BOT_HANDLE"))
	if err != nil {
		log.Printf("Credentials for vip bot not found. Please authenticate!")
	}

	errChan := make(chan error)
	eth, err := contracts.NewETHManager()
	if err != nil {
		log.Fatal(err)
	}

	// Begin submitting transactions to the blockchain
	go eth.Writer.HandleTransactions(errChan)

	startRepl := flag.Bool("repl", false, "Starts the debug REPL")
	job := flag.String("job", "", "Runs a one-off job and exits")
	flag.Parse()

	if *job != "" {
		go errors.LogErrors(errChan)
		runJob(eth, errChan, *job)
		os.Exit(0)
	}

	// Listen for and process any incoming twitter events
	go twitter.MonitorRatelimit()

	twitterEventChan := make(chan *events.TwitterEvent)
	go api.StartServer(eth, twitterEventChan, errChan)
	go events.ProcessTwitterEvents(eth, twitterEventChan, errChan)
	go events.ListenAndRetweet(errChan)

	// Look for any existing applications/challenges that may need to be updated
	go events.ScheduleUpdates(eth, errChan)

	// Start listening for relevant events on the blockchain
	ethEvents := make(chan *events.ETHEvent)
	go events.StartBotListener(eth, ethEvents, errChan)
	go events.ProcessETHEvents(eth, ethEvents, errChan)

	if *startRepl {
		go errors.LogErrors(errChan)
		beginRepl(eth, twitterEventChan, errChan)
	} else {
		errors.LogErrors(errChan)
	}
}
