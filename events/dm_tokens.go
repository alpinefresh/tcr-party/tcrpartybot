package events

import (
	"fmt"
	"log"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
)

const (
	cannotHitFaucetMsg  = "Ack, I can only let you hit the faucet once per day. Try again %s."
	hitFaucetBeginMsg   = "You got it. %d TCRP headed your way, I'll let you know once the transaction is confirmed."
	hitFaucetSuccessMsg = "Done! Your new balance is %d TCRP.\n\nTX Hash: %s"
	balanceMsg          = "Your balance is %d TCRP"
)

func (h *cmdHandler) handleBalance() error {
	if h.Account.MultisigAddress == nil || !h.Account.MultisigAddress.Valid {
		return nil
	}

	balance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	humanBalance := contracts.GetHumanTokenAmount(balance).Int64()
	h.SendDM(fmt.Sprintf(balanceMsg, humanBalance))

	return nil
}

func (h *cmdHandler) handleFaucet() error {
	// Have they hit the faucet recently?
	lastHit, err := models.LatestFaucetHit(h.Account.ID)
	if err != nil {
		return err
	}

	now := time.Now().UTC()
	if lastHit != nil && now.Sub(*lastHit.Timestamp) < 24*time.Hour {
		nextHit := lastHit.Timestamp.Add(24 * time.Hour)
		h.SendDM(fmt.Sprintf(cannotHitFaucetMsg, humanize.Time(nextHit)))
		return nil
	}

	// If they don't yet have a multisig wallet we'll have to stop here
	if !h.Account.MultisigAddress.Valid {
		return fmt.Errorf("Could not faucet tokens to h.Account w/o multisig address: %d", h.Account.ID)
	}

	atomicAmount := contracts.GetAtomicTokenAmount(faucetAmount)
	err = models.RecordFaucetHit(h.Account.ID, atomicAmount)
	if err != nil {
		return errors.Wrap(err)
	}

	h.SendDM(fmt.Sprintf(hitFaucetBeginMsg, faucetAmount))
	tx, err := h.Eth.Writer.SubmitTX(&contracts.TCRPMintTX{
		To:     h.Account.MultisigAddress.String,
		Amount: atomicAmount,
	})
	if err != nil {
		return errors.Wrap(err)
	}

	log.Printf("Faucet hit: %d tokens to %s (%d). TX: %s", faucetAmount, h.Account.TwitterHandle, h.Account.ID, tx.Hex())

	if err = tx.AwaitConfirmation(); err != nil {
		return errors.Wrap(err)
	}

	balance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return errors.Wrap(err)
	}
	humanBalance := contracts.GetHumanTokenAmount(balance).Int64()

	h.SendDM(fmt.Sprintf(hitFaucetSuccessMsg, humanBalance, tx.Hex()))
	return nil
}
