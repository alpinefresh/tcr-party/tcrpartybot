package events

import (
	"log"
	"math/big"
	"math/rand"
	"os"
	"regexp"
	"strings"

	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
	"gitlab.com/alpinefresh/tcrpartybot/twitter"
)

const (
	noAccountMsg       = "Hmm, I haven't met you yet. If you want to join the TCR party send me a tweet that says \"let's party\""
	inactiveAccountMsg = "We're still working on activating your account. Hang tight, we'll message you shortly!"

	awaitingPartyBeginMsg       = "🎉 You're registered to party 🎉. Hang tight while we prepare to distribute our token."
	invalidChallengeResponseMsg = "🙅‍♀️ That's not right! %s"
	nextChallengeMsg            = "Nice, that's it! Here's another one for you: %s"
	preregistrationSuccessMsg   = "🎉 Awesome! You've been registered for the party. We'll reach out once we're ready to distribute TCRP tokens 🎈."
	registrationSuccessMsg      = "🎉 Awesome! Now that you're registered I'll need a few minutes to build your wallet and give you some TCR Party Points to get started with. I'll send you a DM once I'm done."
	invalidCommandMsg           = "Whoops, I don't recognize that command. Try typing help to see what you can say to me."
	helpMsg                     = "Here are the commands I recognize:\n• balance - See your TCRP balance\n• nominate [handle] = Nominate the given Twitter handle to be on the TCR\n• challenge [handle] - Begin a challenge for a listing on the TCR\n• vote [handle] [kick/keep] - Vote on an existing listing's challenge.\n• faucet – Get 100 free tokens per day.\nThose are the basics, but you can check out https://www.tcr.party for more advanced commands."
	errorMsg                    = "Yikes, we ran into an error: %s. Try tweeting at @stevenleeg for help."
	activatingAccountMsg        = "Welcome back to the party! We unfortunately had to reset the TCR after killing Ropsten, but we're glad to see you back. Give me a minute while I rebuild your wallet 👷‍♀️..."

	depositAmount     = 500
	defaultVoteWeight = 50
	faucetAmount      = 100

	twitterHandleRegex = "([_a-zA-Z0-9]{1,15})"
)

// RegistrationEventData collects the required data for keeping track of
// the user registration flow into a struct
type RegistrationEventData struct {
	Event     *TwitterEvent
	Challenge *models.RegistrationChallengeRegistrationQuestion
	Account   *models.Account
}

func parseHandle(handle string) string {
	return strings.ToLower(strings.TrimSpace(strings.Replace(handle, "@", "", -1)))
}

func verifyHandle(handle string) bool {
	nominateMatcher := regexp.MustCompile(twitterHandleRegex)
	return nominateMatcher.MatchString(handle)
}

type cmdHandler struct {
	Account *models.Account
	Eth     *contracts.ETHManager
	ErrChan chan<- error
	Args    []string
}

func (h *cmdHandler) SendDM(msg string) {
	if err := twitter.SendDM(h.Account.TwitterID, msg); err != nil {
		h.ErrChan <- errors.Wrap(err)
		return
	}

	if err := h.Account.UpdateLastDMAt(); err != nil {
		h.ErrChan <- errors.Wrap(err)
		return
	}
}

func (h *cmdHandler) HandleCmd(cmd string) {
	h.Args = strings.FieldsFunc(cmd, func(c rune) bool {
		return c == ' '
	})

	if len(h.Args) == 0 {
		return
	}

	var err error
	switch strings.ToLower(h.Args[0]) {
	case "balance":
		err = h.handleBalance()
	case "nominate":
		err = h.handleNomination()
	case "challenge":
		err = h.handleChallenge()
	case "vote":
		err = h.handleVote()
	case "faucet":
		err = h.handleFaucet()
	case "vote-balance":
		err = h.handleVoteBalance()
	case "vote-deposit":
		err = h.handleVoteDeposit()
	case "vote-withdraw":
		err = h.handleVoteWithdraw()
	case "status":
		err = h.handleStatus()
	case "help":
		h.SendDM(helpMsg)
	default:
		h.SendDM(invalidCommandMsg)
	}

	if err != nil {
		h.ErrChan <- err
	}
}

func processDM(eth *contracts.ETHManager, event *TwitterEvent, errChan chan<- error) {
	log.Printf("Received DM from %s: %s", event.SourceHandle, event.Message)

	err := models.CreateDMAnalyticsEvent(event.SourceID, event.Message)
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}

	// If they don't have an acccount, do nothing.
	account, err := models.FindAccountByTwitterID(event.SourceID)
	if account == nil || err != nil {
		err := twitter.SendDM(event.SourceID, noAccountMsg)
		if err != nil {
			errChan <- errors.Wrap(err)
			return
		}
		return
	}

	handler := cmdHandler{Eth: eth, Account: account, ErrChan: errChan}

	if msg := os.Getenv("MAINTENANCE_MESSAGE"); msg != "" && account.TwitterHandle != "stevenleeg" {
		handler.SendDM(msg)
		return
	}

	// Are they still in the registration challenge stage?
	if account.PassedRegistrationChallengeAt == nil {
		activeChallenge, err := models.FindIncompleteChallenge(account.ID)
		if err != nil {
			errChan <- err
			return
		}

		data := RegistrationEventData{
			Event:     event,
			Challenge: activeChallenge,
			Account:   account,
		}

		if activeChallenge != nil {
			verifyAnswer(eth, data, errChan)
			return
		}

		log.Println("Account has yet to pass activation challenge but does not have any incomplete challenges")
		return
	}

	// They've registered but their account hasn't been activated yet, let's
	// activate it for them.
	if account.ActivatedAt == nil && !account.HasMultisigWallet() {
		identifier := rand.Int63()
		_, err := eth.Writer.SubmitTX(&contracts.WalletFactoryDeployTX{
			Identifier: big.NewInt(identifier),
		})
		if err != nil {
			errChan <- errors.Wrap(err)
			return
		}

		err = account.SetMultisigFactoryIdentifier(identifier)
		if err != nil {
			errChan <- errors.Wrap(err)
			return
		}

		handler.SendDM(activatingAccountMsg)
		return
	} else if account.ActivatedAt == nil {
		handler.SendDM(inactiveAccountMsg)
		return
	}

	// If they haven't been activated yet (ie pre-registration) then we'll
	// stop them here
	if account.ActivatedAt == nil {
		handler.SendDM(preregistrationSuccessMsg)
		return
	}

	handler.HandleCmd(event.Message)
}
