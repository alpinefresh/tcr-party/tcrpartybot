package events

import (
	"fmt"
	"log"
	"math/big"
	"time"

	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
)

// ScheduleUpdates finds new/existing applications/challenge polls and
// schedules tasks to fire their on-chain updater functions once they have
// matured. Example: calling updateStatus on an application once the
// application period has passed.
func ScheduleUpdates(eth *contracts.ETHManager, errChan chan<- error) {
	// First let's instantiate ourselves with existing challenges/applications
	// that need to be scheduled
	listings, err := eth.Reader.GetAllListings()
	if err != nil {
		errChan <- err
		return
	}

	for _, listing := range listings {
		go scheduleListing(eth, listing, errChan)
	}
}

func scheduleUpdateForEvent(eth *contracts.ETHManager, event *ETHEvent, errChan chan<- error) {
	var err error
	var listing *contracts.RegistryListing

	switch event.EventType {
	case ETHEventNewTCRChallenge:
		ethEvent, err := contracts.DecodeChallengeEvent(event.Topics, event.Data)
		if err != nil {
			errChan <- err
			return
		}

		listing, err = eth.Reader.GetListing(ethEvent.ListingHash)
		break
	case ETHEventNewTCRApplication:
		ethEvent, err := contracts.DecodeApplicationEvent(event.Topics, event.Data)
		if err != nil {
			errChan <- err
			return
		}

		listing, err = eth.Reader.GetListing(ethEvent.ListingHash)
		break
	}

	if err != nil {
		errChan <- err
	} else if listing != nil {
		scheduleListing(eth, listing, errChan)
	}
}

func scheduleListing(eth *contracts.ETHManager, application *contracts.RegistryListing, errChan chan<- error) {
	hasOpenChallenge := false

	if application.ChallengeID.Cmp(big.NewInt(0)) != 0 {
		challenge, err := eth.Reader.GetChallenge(application.ChallengeID)
		if err != nil {
			errChan <- err
			return
		}
		hasOpenChallenge = challenge.Resolved == false
	}

	twitterHandle, err := eth.Reader.GetListingDataFromHash(application.ListingHash)
	if err != nil {
		errChan <- err
		return
	}

	if !application.Whitelisted && !hasOpenChallenge {
		// This listing hasn't been whitelisted yet and doesn't have an open
		// challenge. This means we'll need to schedule a updateStatus task
		expirationTime := time.Unix(application.ApplicationExpiry.Int64(), 0)
		if expirationTime.After(time.Now()) {
			log.Printf("[updater] Application @%s is in progress. Sleeping until %s", twitterHandle, expirationTime.Format(time.UnixDate))
			time.Sleep(time.Until(expirationTime) + (30 * time.Second))
		}

		updateStatus(eth, application, errChan)
	} else if hasOpenChallenge {
		// The listing has an open challenge, meaning we'll need to schedule
		// tasks to reveal any votes and update the status
		poll, err := eth.Reader.GetPoll(application.ChallengeID)
		if err != nil {
			errChan <- err
			return
		}

		commitEndTime := time.Unix(poll.CommitEndDate.Int64(), 0)
		revealEndTime := time.Unix(poll.RevealEndDate.Int64(), 0)
		if commitEndTime.After(time.Now()) {
			// We haven't yet hit the commit time, so let's sleep until we do
			// and then reveal the vote
			log.Printf("[updater] Challenge @%s is in commit. Sleeping until %s", twitterHandle, commitEndTime.Format(time.UnixDate))
			time.Sleep(time.Until(commitEndTime) + (30 * time.Second))
		}

		if revealEndTime.After(time.Now()) {
			reveal(eth, application, errChan)
			if err != nil {
				errChan <- err
				return
			}
			log.Printf("[updater] Challenge @%s is in reveal. Sleeping until %s", twitterHandle, revealEndTime.Format(time.UnixDate))
			time.Sleep(time.Until(revealEndTime) + (30 * time.Second))
		}

		if revealEndTime.Before(time.Now()) {
			updateStatus(eth, application, errChan)
		}
	}

	// Fallthrough case is for applications that are whitelisted and have no
	// open challenges (we don't need to do anything for them)
}

func reveal(eth *contracts.ETHManager, application *contracts.RegistryListing, errChan chan<- error) {
	votes, err := models.FindUnrevealedVotesFromPoll(application.ChallengeID.Int64())
	if err != nil {
		errChan <- err
		return
	}

	log.Printf("[updater] Revealing %d votes on poll %s.", len(votes), application.ChallengeID.String())
	for _, vote := range votes {
		account, err := models.FindAccountByID(vote.AccountID)
		if err != nil {
			errChan <- errors.Wrap(err)
			continue
		} else if account == nil {
			errChan <- fmt.Errorf("Could not find account for ID %d", vote.AccountID)
			continue
		}

		log.Printf("\tRevealing %t vote by %s (w: %d)", vote.Vote, account.TwitterHandle, vote.Weight)
		tx, err := eth.Writer.SubmitTX(&contracts.PLCRRevealVoteTX{
			MultisigAddress: account.MultisigAddress.String,
			PollID:          application.ChallengeID,
			Vote:            vote.Vote,
			Salt:            big.NewInt(vote.Salt),
		})
		if err != nil {
			errChan <- errors.Wrap(err)
			continue
		}

		if tx.AwaitConfirmation(); err != nil {
			errChan <- errors.Wrap(err)
			return
		}

		err = vote.MarkRevealed()
		if err != nil {
			errChan <- errors.Wrap(err)
		}
	}
}

func updateStatus(eth *contracts.ETHManager, application *contracts.RegistryListing, errChan chan<- error) {
	twitterHandle, err := eth.Reader.GetListingDataFromHash(application.ListingHash)
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}
	log.Printf("[updater] Attempting to updateStatus of listing %s", twitterHandle)

	// Refresh the listing, just in case there was a delay before calling this
	// function
	application, err = eth.Reader.GetListing(application.ListingHash)
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}

	// Reschedule if they have an ongoing challenge being waged against them
	// and it's not yet reveal time
	if application.ChallengeID.Cmp(big.NewInt(0)) != 0 {
		poll, err := eth.Reader.GetPoll(application.ChallengeID)
		if err != nil {
			errChan <- errors.Wrap(err)
			return
		}

		revealEndTime := time.Unix(poll.RevealEndDate.Int64(), 0)
		if revealEndTime.After(time.Now()) {
			go scheduleListing(eth, application, errChan)
			return
		}
	}

	tx, err := eth.Writer.SubmitTX(&contracts.RegistryUpdateStatusTX{
		ListingHash: application.ListingHash,
	})
	if err != nil {
		errChan <- errors.Wrap(err)
		return
	}
	log.Printf("[updater] Done! Updating tx: %s", tx.Hex())
}
