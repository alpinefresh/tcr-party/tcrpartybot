package events

import (
	"fmt"
	"github.com/dustin/go-humanize"
	"log"
	"math/big"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"gitlab.com/alpinefresh/tcrpartybot/contracts"
	"gitlab.com/alpinefresh/tcrpartybot/errors"
	"gitlab.com/alpinefresh/tcrpartybot/models"
)

const (
	votingArgErrorMsg                  = "Whoops, looks like you forgot something. Try again with something like 'vote [twitter handle] kick' or 'vote [twitter handle] keep [vote weight, default 50]'"
	votingListingNotFoundMsg           = "Hmm, I couldn't find a registry listing for that twitter handle. Are you sure they've been nominated to the registry?"
	votingChallengeNotFoundMsg         = "Looks like that twitter handle doesn't have a challenge opened on it yet. If you'd like to challenge their place on the registry respond with 'challenge %s'."
	votingChallengeErrorMsg            = "There was an error committing your vote. The admins have been notified!"
	votedAlreadyMsg                    = "Oops! Looks like you've already voted %s on this challenge."
	votingEndedMsg                     = "Ack! Looks like the voting period has ended for this challenge. Hang tight, we'll announce the result on %s."
	votingBeginMsg                     = "We've submitted your vote. Hang tight, we'll notify you once everything is confirmed."
	votingSuccessMsg                   = "Your vote to %s %s's listing with a weight of %d has been confirmed!\n\nWe'll announce the results on %s.\n\nTx hash: %s"
	voteInsufficientFundsMsg           = "You don't have enough funds locked up to vote with a weight of %d. You currently have %d available. If you would like to lock up more tokens to increase your voting weight, reply with vote-deposit [amount]. 50 is usually a good starting number.\n\nRemember that any tokens you lock up for voting will be unavailable for use in nominations/challenges."
	voteBalanceMsg                     = "You have %d tokens deposited to vote. This means you can vote with a maximum weight of %d."
	voteLockedMsg                      = "\n\nSince you have voted on @%s's challenge, you are currently able to withdraw %d of your voting balance. These tokens will become available to withdraw once the challenge has completed %s."
	voteAllLockedMsg                   = "\n\nSince you put your full voting weight on @%s's challenge, you will not be able to withdraw any tokens into your wallet balance until the challenge completes %s."
	plcrWithdrawArgErrorMsg            = "Whoops, looks like you forgot something. Try vote-withdraw [amount]"
	plcrDepositArgErrorMsg             = "Whoops, looks like you forgot something. Try vote-deposit [amount]"
	plcrDepositInsufficientFundsMsg    = "Whoops, looks like you don't have enough tokens to deposit this amount to your maximum voting weight. Your current balance is %d"
	plcrDepositBeginMsg                = "I've submitted your tokens for deposit. Hang tight, I'll let you know when everything clears."
	plcrDepositSuccessMsg              = "Your tokens have been deposited successfully!\n\nYou now have %d tokens locked up to vote and %d tokens in your wallet.\n\nTX hash: %s"
	plcrWithdrawInsufficientFunds      = "Whoops, you only have %d tokens locked up."
	plcrWithdrawBeginMsg               = "I've submitted your request to withdraw. Hang tight, I'll let you know when everything clears."
	plcrWithdrawSuccessMsg             = "Your tokens have been withdrawn successfully!\n\nYou now have %d tokens locked up to vote and %d tokens in your wallet.\n\nTX hash: %s"
	plcrLockedTokensMsg                = "Whoops! You currently have %d tokens locked up in existing challenge votes. These tokens cannot be withdrawn until challenge for %s's listing has completed (%s)."
	plcrLockedTokenWithoutChallengeMsg = "Whoops! You currently have %d tokens locked up in existing challenge votes. You'll need to wait for all of the challenges are resolved before you can withdraw these tokens."
	invalidNumberMsg                   = "That doesn't look like a valid number to deposit... Get outta here."
)

func (h *cmdHandler) handleVote() error {
	if len(h.Args) < 3 {
		h.SendDM(votingArgErrorMsg)
		return nil
	}

	voteDirection := strings.ToLower(h.Args[2])
	if voteDirection != "keep" && voteDirection != "kick" {
		h.SendDM(votingArgErrorMsg)
		return nil
	}

	if h.Account.MultisigAddress == nil || !h.Account.MultisigAddress.Valid {
		return errors.New("User attempted to vote without a multisig address")
	}

	// Fetch their PLCR deposit
	balance, err := h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	weight := balance
	humanWeight := contracts.GetHumanTokenAmount(weight).Int64()

	// If their PLCR deposit is 0 maybe we can help them out
	if balance.Cmp(big.NewInt(0)) == 0 {
		walletBalance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
		if err != nil {
			return err
		}

		// They don't even have enough tokens in their balance, let's stop
		compare := walletBalance.Cmp(big.NewInt(defaultVoteWeight))
		if compare == -1 {
			msg := fmt.Sprintf(voteInsufficientFundsMsg, humanWeight, contracts.GetHumanTokenAmount(balance).Int64())
			h.SendDM(msg)
			return nil
		}

		// Cool, let's put some tokens in their h.Account to vote with
		toDeposit := contracts.GetAtomicTokenAmount(defaultVoteWeight)
		tx, err := h.Eth.Writer.SubmitTX(&contracts.PLCRDepositTX{
			MultisigAddress: h.Account.MultisigAddress.String,
			Amount:          toDeposit,
		})
		if err != nil {
			h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
			return err
		}

		log.Printf("%s did not have any tokens for voting, depositing on their behalf (tx: %s)", h.Account.TwitterHandle, tx.Hex())
		if err := tx.AwaitConfirmation(); err != nil {
			return errors.Wrap(err)
		}

		// Fetch their balance again to make sure we're all good
		balance, err = h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
		if err != nil {
			return err
		}
	}

	handle := parseHandle(h.Args[1])
	if len(h.Args) > 3 {
		intWeight, err := strconv.ParseInt(h.Args[3], 10, 64)
		if err != nil {
			intWeight = 50
		}

		weight = contracts.GetAtomicTokenAmount(intWeight)
	}

	// Make sure their weight is within their means
	humanWeight = contracts.GetHumanTokenAmount(weight).Int64()
	if balance.Cmp(weight) == -1 {
		msg := fmt.Sprintf(voteInsufficientFundsMsg, humanWeight, contracts.GetHumanTokenAmount(balance).Int64())
		h.SendDM(msg)
		return nil
	}

	// Check to make sure there is an active poll for the given listing
	listing, err := h.Eth.Reader.GetListingFromHandle(handle)
	if err != nil {
		return err
	} else if listing == nil {
		// Check to see if the listing is using an @
		listing, err = h.Eth.Reader.GetListingFromHandle("@" + handle)
		if err != nil {
			return err
		} else if listing == nil {
			h.SendDM(votingListingNotFoundMsg)
			return nil
		}
	}

	if listing.ChallengeID.Cmp(big.NewInt(0)) == 0 {
		h.SendDM(fmt.Sprintf(votingChallengeNotFoundMsg, handle))
		return nil
	}

	// Fetch the poll they want to vote on
	poll, err := h.Eth.Reader.GetPoll(listing.ChallengeID)
	if err != nil {
		return err
	} else if poll == nil {
		log.Printf("Poll doesn't exist for listing: %x, challenge: %d", listing.ListingHash, listing.ChallengeID)
		h.SendDM(votingChallengeErrorMsg)
		return err
	}

	commitEndDate := time.Unix(poll.CommitEndDate.Int64(), 0)
	revealDate := time.Unix(poll.RevealEndDate.Int64(), 0)
	fmtRevealDate := revealDate.Format(time.RFC1123)

	// Make sure we're still in the commit phase
	if commitEndDate.Before(time.Now()) {
		h.SendDM(fmt.Sprintf(votingEndedMsg, fmtRevealDate))
		return nil
	}

	// Make sure they aren't voting twice
	vote, err := models.FindVote(listing.ChallengeID.Int64(), h.Account.ID)
	if err != nil {
		return err
	} else if vote != nil {
		voteValue := "keep"
		if !vote.Vote {
			voteValue = "kick"
		}
		h.SendDM(fmt.Sprintf(votedAlreadyMsg, voteValue))
		return nil
	}

	h.SendDM(votingBeginMsg)

	voteValue := voteDirection == "keep"
	salt := rand.Int63()
	tx, err := h.Eth.Writer.SubmitTX(&contracts.PLCRCommitVoteTX{
		MultisigAddress: h.Account.MultisigAddress.String,
		PollID:          listing.ChallengeID,
		Amount:          weight,
		Salt:            big.NewInt(salt),
		Vote:            voteValue,
	})
	if err != nil {
		return err
	}

	// Wait for the vote to clear
	if err := tx.AwaitConfirmation(); err != nil {
		return err
	}

	humanWeight = contracts.GetHumanTokenAmount(weight).Int64()
	_, err = models.CreateVote(h.Account, listing.ChallengeID.Int64(), salt, voteValue, humanWeight)
	if err != nil {
		return err
	}

	h.SendDM(fmt.Sprintf(votingSuccessMsg, voteDirection, handle, humanWeight, fmtRevealDate, tx.Hex()))
	return nil
}

func (h *cmdHandler) handleVoteBalance() error {
	if h.Account.MultisigAddress == nil || !h.Account.MultisigAddress.Valid {
		err := errors.New("User attempted to fetch PLCR balance without a multisig address")
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}

	balance, err := h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}

	lockedTokens, err := h.Eth.Reader.PLCRLockedTokens(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	unlockedTokens := new(big.Int)
	unlockedTokens.Sub(balance, lockedTokens)

	humanBalance := contracts.GetHumanTokenAmount(balance).Int64()
	humanUnlocked := contracts.GetHumanTokenAmount(unlockedTokens).Int64()

	msg := fmt.Sprintf(voteBalanceMsg, humanBalance, humanBalance)

	if lockedTokens.Cmp(big.NewInt(0)) != 0 {
		// Get the challenge that the tokens are locked up in
		challengeID, err := h.Eth.Reader.PLCRLockedChallengeID(h.Account.MultisigAddress.String)
		if err != nil {
			return err
		}

		// Get the challenge
		challenge, err := models.FindRegistryChallengeByPollID(challengeID.Int64())
		if err != nil {
			return err
		} else if challenge == nil {
			h.SendDM(msg)
			return err
		}

		challengeHandle := challenge.Listing.TwitterHandle
		resolveAt := humanize.Time(*challenge.CommitEndsAt)

		if unlockedTokens.Cmp(big.NewInt(0)) == 0 {
			msg += fmt.Sprintf(voteAllLockedMsg, challengeHandle, resolveAt)
		} else {
			msg += fmt.Sprintf(voteLockedMsg, challengeHandle, humanUnlocked, resolveAt)
		}
	}

	h.SendDM(msg)
	return nil
}

func (h *cmdHandler) handleVoteDeposit() error {
	if len(h.Args) < 2 {
		h.SendDM(plcrDepositArgErrorMsg)
		return nil
	}

	if !h.Account.MultisigAddress.Valid {
		err := errors.New("User attempted to PLCRDeposit without a multisig address")
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}

	amount, err := strconv.ParseInt(h.Args[1], 10, 64)
	if err != nil {
		h.SendDM(invalidNumberMsg)
		return nil
	} else if amount <= 0 {
		h.SendDM(invalidNumberMsg)
		return nil
	}

	toDeposit := contracts.GetAtomicTokenAmount(amount)

	balance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	if balance.Cmp(toDeposit) == -1 {
		msg := fmt.Sprintf(plcrDepositInsufficientFundsMsg, contracts.GetHumanTokenAmount(balance).Int64())
		h.SendDM(msg)
		return nil
	}

	h.SendDM(plcrDepositBeginMsg)
	tx, err := h.Eth.Writer.SubmitTX(&contracts.PLCRDepositTX{
		MultisigAddress: h.Account.MultisigAddress.String,
		Amount:          toDeposit,
	})
	if err != nil {
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}

	// Wait for the deposit to complete
	if err := tx.AwaitConfirmation(); err != nil {
		return err
	}

	// Get their new balance
	plcrBalance, err := h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}
	humanPLCRBalance := contracts.GetHumanTokenAmount(plcrBalance)

	walletBalance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}
	humanWalletBalance := contracts.GetHumanTokenAmount(walletBalance)

	h.SendDM(fmt.Sprintf(plcrDepositSuccessMsg, humanPLCRBalance, humanWalletBalance, tx.Hex()))
	return nil
}

func (h *cmdHandler) handleVoteWithdraw() error {
	if len(h.Args) < 2 {
		h.SendDM(plcrWithdrawArgErrorMsg)
		return nil
	}

	if !h.Account.MultisigAddress.Valid {
		err := errors.New("User attempted to PLCRwithdraw without a multisig address")
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}
	amount, err := strconv.ParseInt(h.Args[1], 10, 64)
	if err != nil {
		h.SendDM(invalidNumberMsg)
		return nil
	} else if amount <= 0 {
		h.SendDM(invalidNumberMsg)
		return nil
	}

	toWithdraw := contracts.GetAtomicTokenAmount(amount)

	balance, err := h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	if balance.Cmp(toWithdraw) == -1 {
		msg := fmt.Sprintf(plcrWithdrawInsufficientFunds, contracts.GetHumanTokenAmount(balance).Int64())
		h.SendDM(msg)
		return nil
	}

	// Are their tokens locked up in challenges?
	lockedTokens, err := h.Eth.Reader.PLCRLockedTokens(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}

	if lockedTokens.Cmp(toWithdraw) == 1 || lockedTokens.Cmp(toWithdraw) == 0 {
		challengeID, err := h.Eth.Reader.PLCRLockedChallengeID(h.Account.MultisigAddress.String)
		if err != nil {
			return err
		}

		// Get the challenge
		challenge, err := models.FindRegistryChallengeByPollID(challengeID.Int64())
		if err != nil {
			return err
		} else if challenge == nil {
			msg := fmt.Sprintf(plcrLockedTokenWithoutChallengeMsg, contracts.GetHumanTokenAmount(lockedTokens).Int64())
			h.SendDM(msg)
			return err
		}

		lockedTokenAmt := contracts.GetHumanTokenAmount(lockedTokens).Int64()
		challengeHandle := challenge.Listing.TwitterHandle
		resolveAt := humanize.Time(*challenge.CommitEndsAt)

		msg := fmt.Sprintf(plcrLockedTokensMsg, lockedTokenAmt, challengeHandle, resolveAt)
		h.SendDM(msg)
		return nil
	}

	h.SendDM(plcrWithdrawBeginMsg)
	tx, err := h.Eth.Writer.SubmitTX(&contracts.PLCRWithdrawTX{
		MultisigAddress: h.Account.MultisigAddress.String,
		Amount:          toWithdraw,
	})
	if err != nil {
		h.SendDM(fmt.Sprintf(errorMsg, err.Error()))
		return err
	}

	// Wait for the deposit to complete
	if err := tx.AwaitConfirmation(); err != nil {
		return err
	}

	// Get their new balance
	plcrBalance, err := h.Eth.Reader.PLCRGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}
	humanPLCRBalance := contracts.GetHumanTokenAmount(plcrBalance)

	walletBalance, err := h.Eth.Reader.TCRPGetBalance(h.Account.MultisigAddress.String)
	if err != nil {
		return err
	}
	humanWalletBalance := contracts.GetHumanTokenAmount(walletBalance)

	h.SendDM(fmt.Sprintf(plcrWithdrawSuccessMsg, humanPLCRBalance.Int64(), humanWalletBalance.Int64(), tx.Hex()))
	return nil
}
