package events

import (
	"log"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/alpinefresh/tcrpartybot/contracts"
)

// EventType is an alias type for event constants' values
type twitterEventType string

const (
	TwitterEventTypeMention       twitterEventType = "EventTypeMention"
	TwitterEventTypeDM            twitterEventType = "EventTypeDM"
	TwitterEventTypeVote          twitterEventType = "EventTypeVote"
	TwitterEventTypePollCompleted twitterEventType = "EventTypePollCompleted"
	TwitterEventTypeFollow        twitterEventType = "EventTypeFollow"

	// ETHEventNewMultisigWallet is triggered when the multisig wallet factory instantiates a new wallet
	ETHEventNewMultisigWallet         = "ContractInstantiation"
	ETHEventNewMultisigSubmission     = "Submission"
	ETHEventNewTCRApplication         = "_Application"
	ETHEventNewTCRChallenge           = "_Challenge"
	ETHEventTCRDeposit                = "_Deposit"
	ETHEventTCRWithdrawal             = "_Withdrawal"
	ETHEventTCRApplicationWhitelisted = "_ApplicationWhitelisted"
	ETHEventTCRApplicationRemoved     = "_ApplicationRemoved"
	ETHEventTCRListingRemoved         = "_ListingRemoved"
	ETHEventTCRListingWithdrawn       = "_ListingWithdrawn"
	ETHEventTCRTouchAndRemoved        = "_TouchAndRemoved"
	ETHEventTCRChallengeFailed        = "_ChallengeFailed"
	ETHEventTCRChallengeSucceeded     = "_ChallengeSucceeded"
	ETHEventTCRRewardClaimed          = "_RewardClaimed"
	ETHEventTCRExitInitialized        = "_ExitInitialized"

	ETHEventTokenTransfer     = "Transfer"
	ETHEventTokenApproval     = "Approval"
	ETHEventTokenMint         = "Mint"
	ETHEventTokenMintFinished = "MintFinished"

	ETHEventPLCRVoteCommitted         = "_VoteCommitted"
	ETHEventPLCRPollCreated           = "_PollCreated"
	ETHEventPLCRVoteRevealed          = "_VoteRevealed"
	ETHEventPLCRVotingRightsGranted   = "_VotingRightsGranted"
	ETHEventPLCRVotingRightsWithdrawn = "_VotingRightsWithdrawn"
	ETHEventPLCRTokensRescued         = "_TokensRescued"
)

// TwitterEvent represents an incoming event from Twitter
type TwitterEvent struct {
	EventType    twitterEventType // type of event
	Time         time.Time        // timestamp
	ObjectID     string           // ID of incoming event's object (ie a tweet ID for a mention)
	SourceHandle string           // twitter handle sending
	SourceID     int64            // twitter ID of the handle
	Message      string           // whole message
}

// ETHEvent represents an incoming event from the blockchain
type ETHEvent struct {
	EventType   string
	CreatedAt   *time.Time
	BlockNumber uint64
	TxIndex     uint
	LogIndex    uint
	TxHash      string
	Data        []byte
	Topics      []common.Hash
}

// ProcessTwitterEvents listens for twitter events and fires of a corresponding handler
func ProcessTwitterEvents(eth *contracts.ETHManager, eventChan <-chan *TwitterEvent, errorChan chan<- error) {
	for {
		event := <-eventChan
		switch event.EventType {
		case TwitterEventTypeMention:
			go processMention(eth, event, errorChan)
			break

		case TwitterEventTypeFollow:
			go processFollow(eth, event, errorChan)
			break

		case TwitterEventTypeDM:
			go processDM(eth, event, errorChan)
			break
		}
	}
}

// ProcessETHEvents listens for blockchain events and fires a corresponding handler
func ProcessETHEvents(eth *contracts.ETHManager, eventChan <-chan *ETHEvent, errChan chan<- error) {
	var err error

	handler := &ethEventHandler{eth, errChan}

	for {
		event := <-eventChan
		go scheduleUpdateForEvent(eth, event, errChan)

		log.Printf("Found event %s", event.EventType)
		switch event.EventType {
		case ETHEventNewMultisigWallet:
			err = handler.ProcessMultisigWalletCreation(event)
			break
		case ETHEventNewTCRApplication:
			err = handler.ProcessNewApplication(event)
			break
		case ETHEventTCRApplicationWhitelisted:
			err = handler.ProcessApplicationWhitelisted(event)
			break
		case ETHEventTCRApplicationRemoved:
			err = handler.ProcessApplicationRemoved(event)
			break
		case ETHEventTCRChallengeSucceeded:
			err = handler.ProcessChallengeSucceeded(event)
			break
		case ETHEventTCRChallengeFailed:
			err = handler.ProcessChallengeFailed(event)
			break
		case ETHEventNewTCRChallenge:
			err = handler.ProcessNewChallenge(event)
			break
		case ETHEventTCRWithdrawal:
			err = handler.ProcessWithdrawal(event)
			break
		case ETHEventTCRRewardClaimed:
			err = handler.ProcessRewardClaimed(event)
			break
		}

		if err != nil {
			errChan <- err
		}
	}
}
