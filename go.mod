module gitlab.com/alpinefresh/tcrpartybot

require (
	github.com/allegro/bigcache v1.2.0 // indirect
	github.com/aristanetworks/goarista v0.0.0-20190219163901-728bce664cf5 // indirect
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/deckarep/golang-set v1.7.1
	github.com/dghubble/oauth1 v0.5.0
	github.com/dghubble/sling v1.2.0 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/ethereum/go-ethereum v1.8.23
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/rjeczalik/notify v0.9.2 // indirect
	github.com/rs/cors v1.6.0 // indirect
	github.com/stevenleeg/go-twitter v0.0.0-20190219173544-819ddce06dbe
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95 // indirect
)
