FROM golang:1.11.1-stretch

WORKDIR /app
COPY . .

ENV GO111MODULE=on

WORKDIR tcrpartybot
RUN go get

RUN go build -o tcrpartybot *.go
RUN cp tcrpartybot /usr/bin/tcrpartybot

CMD tcrpartybot
