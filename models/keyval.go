package models

import (
	"database/sql"
	"fmt"
	"strconv"
)

const (
	LatestListSyncedEventKey     = "LatestListSyncedEvent"
	LatestRegistrySyncedEventKey = "LatestRegistrySyncedEvent"
	LatestSyncedBlockKey         = "LatestSyncedBlock"
	LatestLoggedBlockKey         = "LatestLoggedBlock"
	LatestSyncedTweetKey         = "LatestSyncedTweet"
	TwitterRequestTokenKey       = "TwitterRequestToken"
)

type keyValueRow struct {
	Key   string `db:"key"`
	Value string `db:"value"`
}

// SetKey updates or inserts a key with the provided value
func SetKey(key string, value string) error {
	db := GetDBSession()

	result := db.MustExec(`
		INSERT INTO keyval_store (key, value) VALUES($1, $2)
		ON CONFLICT (key) DO UPDATE SET value=$2
	`, key, value)

	_, err := result.RowsAffected()
	return err
}

// GetKey fetches the given key's value from the database
func GetKey(key string) (string, error) {
	db := GetDBSession()

	row := &keyValueRow{}
	err := db.Get(row, "SELECT * FROM keyval_store WHERE key=$1 LIMIT 1", key)

	if err != nil && err != sql.ErrNoRows {
		return "", err
	}

	return row.Value, nil
}

// FetchInt fetches the given key and parses it into an int64
func FetchInt(key string) (int64, error) {
	val, err := GetKey(key)
	if err != nil {
		return 0, err
	} else if val == "" {
		return 0, nil
	}

	return strconv.ParseInt(val, 10, 64)
}

// SetInt converts the value to a string and inserts it into the given key
func SetInt(key string, val int64) error {
	return SetKey(key, fmt.Sprintf("%d", val))
}

// ClearKey removes the given key's row from the database
func ClearKey(key string) error {
	db := GetDBSession()

	row := &keyValueRow{}
	err := db.Get(row, "DELETE FROM keyval_store WHERE key=$1", key)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	return nil
}
