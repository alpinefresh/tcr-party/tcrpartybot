# TCR Party Bot!

This repository implements the brains behind the TCR Party Bot. This bot
facilitates all interactions between Twitter and the underlying smart contracts
on the blockchain in addition to retweeting all members of the live TCR.

## Contributing
Since you're reading this you've probably realized that TCR Party is an open
source project! If you're feeling particularly frisky and have a feature that
you'd like to see added or a bug that needs to be fixed we'd love for you to
contribute.

The best place to get an inside look into our development process is by
checking out our issue [board](https://gitlab.com/alpinefresh/tcr-party/tcrpartybot/boards) and [list](https://gitlab.com/alpinefresh/tcr-party/tcrpartybot/issues), which
keep track of tasks we're looking to complete. If you notice your request
hasn't been already mentioned, feel free to open a ticket to begin discussion.
We'd ask that you **please open a ticket to discuss feature requests/issues
before opening a PR**, as we would like to make sure that there is no
wasted/duplicated efforts. Nobody likes spending hours building a PR just to
see it go stale in the tracker!

Additionally, we require all discussions around TCR Party follow Recurse
Center's wonderful [code of conduct](https://www.recurse.com/code-of-conduct)
in order to ensure a welcoming and fun environment for everybody.

## Dev environment setup
### 1. Prepare dependencies
In order to get started you'll need to first deploy our contracts to a local
blockchain. You can find more info on how to do this in the
[contracts repository](https://gitlab.com/alpinefresh/tcr-party/contracts).

Additionally, you'll need to have a working installation of
[Golang](https://golang.org/) and a local Postgres database to work with.

### 2. Set up your environment file
We'll need to set up some variables dependant on your environment to get
started. Create a new file in your cloned repository within `root/tcrpartybot/.env` with
the following contents:

```bash
API_TOKEN_HASH=''
VIP_BOT_HANDLE=
PARTY_BOT_HANDLE=

DATABASE_URL=postgres://localhost:5432/tcrparty?sslmode=disable
TWITTER_CONSUMER_KEY=
TWITTER_CONSUMER_SECRET=
TWITTER_ENV=dev

SEND_TWITTER_INTERACTIONS=false

PREREGISTRATION=false
INITIAL_DISTRIBUTION_AMOUNT=3000
GAS_MODIFIER=0
TWITTER_LIST_SLUG=tcr-party-dev

BASE_URL=https://bbd828f3.ngrok.io
SERVER_HOST=0.0.0.0:8080

PORT=8080

ETH_NODE_URI=http://127.0.0.1:7545

MASTER_PRIVATE_KEY=

TOKEN_ADDRESS=
WALLET_FACTORY_ADDRESS=
TCR_ADDRESS=
START_BLOCK=0
```

A few notes on the fields:
* `API_TOKEN_HASH`: Generate a hash using [this tool](https://bcrypt-generator.com/). You will use this string to authenticate later.
In the .env put 'single quotes' around the string to allow it to be parsed correctly.
* `VIP_BOT_HANDLE` and `VIP_BOT_HANDLE`: These should both be your personal twitter handle for testing.
* `TWITTER_CONSUMER_KEY` and `TWITTER_CONSUMER_SECRET`: If you want to test with Twitter
you'll need to get a consumer key, secret, and env from [their developer portal](https://developer.twitter.com).
This is an annoying process and isn't necessary to develop, so don't worry about it if you
just want to make some smaller changes or play around.
* `SEND_TWITTER_INTERACTIONS`: When set to false, this will tell the bot to avoid attempting
to send data to Twitter's API and instead just echo interactions to the console for debugging purposes.
* `TWITTER_LIST_SLUG`: In your twitter account you need to *create a new (empty) list named tcr-party-dev* too.
* `PORT`: This is the port where your server will be hosted locally.
* `ETH_NODE_URI`: This is the URI where your local blockchain can be found. This example is the default URI for Ganache.
* `MASTER_PRIVATE_KEY` should be a key (WITHOUT the leading 0x) with some ETH in its balance,
otherwise the bot will run into issues when creating transactions. Ganache provides a list of
addresses with 100ETH when you start your development chain. The private key of a Ganache account
is found by clicking on the key symbol on the right hand side.
* `TOKEN_ADDRESS`, `WALLET_FACTORY_ADDRESS` and `TCR_ADDRESS`: You need to populate these using
the contract addresses that are spat out when you deploy [your contracts](https://gitlab.com/alpinefresh/tcr-party/contracts).
Look for the addresses specified by `TCRPartyPoints:`, `MultiSigWalletFactory:` and `Registry:`.
* `START_BLOCK`: You can leave this at 0 unless you're seeing performance issues when running
the initial sync (i.e. if you're deploying to mainnet or testnet).

### 3. Database setup
Connect to your postgres instance and create a new database entitled `tcrparty`:

```SQL
CREATE DATABASE tcrparty;
```

Ensure whatever role you're using has permission to connect to and modify this database:

```SQL
GRANT ALL PRIVILEGES ON DATABASE tcrparty TO steve;
```

Now let's set up our schema! We use [Migrate](https://github.com/golang-migrate/migrate/tree/master/cli) for
database migrations, which you can find installation instructions for in their
README. You can find a conveniently preconfigured script for running migration
commands in `bin/migrate`.

Once you have migrate set up, you can set up your database by running:

```bash
$ bin/migrate up
```

### 5. Starting the server and authenticating your twitter account
Once you have everything set up, the final thing you need to do is authenticate your twitter account.
(reminder: did you clone the contracts repository and deploy them onto your local Ganache instance?)

1. You should be able to run the party bot by `cd`ing into the `tcrpartybot` directory and running:

```bash
$ go run *.go -repl
```

Notice the `-repl` flag, which tells the bot to start an interactive command
line for you to simulate various interactions such as DMs, mentions, etc.
without needing to use Twitter's API.

This command will currently give you errors as the `VIP_BOT_HANDLE` and `PARTY_BOT_HANDLE` haven't been authenticated.

2. With your server running on one terminal, open another terminal and run the following command.
* Replace the string 'abc123' with the string you used to generate your `API_TOKEN_HASH`
* If you changed `PORT` from being 8080, update the port in URL below

```bash
$ curl --header "Authorization:abc123" --data "" http://127.0.0.1:8080/admin/authenticate-vip
```

3. Visit this URL the command returns, log into the account you used for `VIP_BOT_HANDLE`,
approve the access and copy down the PIN that is provided to you.

4. Now run the exact same command again from your terminal, but fill in the data field with your pin.
This should produce a 'Success' message.

5. Repeat steps 2-4 but with the URL ending in `/admin/authenticate-party` instead of `vip`

6. Stop your server running and rerun the following command, now with twitter authenticated
```bash
$ go run *.go -repl
```